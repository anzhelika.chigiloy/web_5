<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['bd'] = !empty($_COOKIE['bd_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['accept'] = !empty($_COOKIE['accept_error']);
    $errors['abilities'] = !empty($_COOKIE['abilities_error']);

    if (!empty($errors['fio'])) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Заполните email.</div>';
    }

    if ($errors['bd']) {
        setcookie('bd_error', '', 100000);
        $messages[] = '<div class="error">Заполните date.</div>';
    }

    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">pull bio.</div>';
    }

    if ($errors['sex']) {
        setcookie('sex_error', '', 100000);
        $messages[] = '<div class="error">pull sex.</div>';
    }

    if ($errors['limbs']) {
        setcookie('limbs_error', '', 100000);
        $messages[] = '<div class="error">pull limbs.</div>';
    }

    if ($errors['abilities']) {
        setcookie('abilities_error', '', 100000);
        $messages[] = '<div class="error">abilities error.</div>';
    }

    if ($errors['accept']) {
        setcookie('accept_error', '', 100000);
        $messages[] = '<div class="error">accept error.</div>';
    }


    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['bd'] = empty($_COOKIE['bd_value']) ? '' : strip_tags($_COOKIE['bd_value']);
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
    $values['accept'] = empty($_COOKIE['accept_value']) ? '' : strip_tags($_COOKIE['accept_value']);
    $values['abilities'] = array();
    $values['abilities'][0] = empty($_COOKIE['abilities_value_1']) ? '' : $_COOKIE['abilities_value_1'];
    $values['abilities'][1] = empty($_COOKIE['abilities_value_2']) ? '' : $_COOKIE['abilities_value_2'];
    $values['abilities'][2] = empty($_COOKIE['abilities_value_3']) ? '' : $_COOKIE['abilities_value_3'];
    $values['abilities'][3] = empty($_COOKIE['abilities_value_4']) ? '' : $_COOKIE['abilities_value_4'];

    session_start();
    if (!empty($_GET['quit'])) {
        session_destroy();
        $_SESSION['login'] = '';
    }

  if (!empty($_SESSION['login'])) {
      $user = 'u20704';
      $pass_db = '6653476';
      $db = new PDO('mysql:host=localhost;dbname=u20704', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
      $stmt1 = $db->prepare('SELECT fio, email, bd, sex, limbs, bio FROM formtab5 WHERE form_id = ?');
      $stmt1->execute([$_SESSION['uid']]);
      $row = $stmt1->fetch(PDO::FETCH_ASSOC);
      $values['fio'] = strip_tags($row['fio']);
      $values['email'] = strip_tags($row['email']);
      $values['bd'] = strip_tags($row['bd']);
      $values['sex'] = strip_tags($row['sex']);
      $values['limbs'] = strip_tags($row['limbs']);
      $values['bio'] = strip_tags($row['bio']);

      $stmt2 = $db->prepare('SELECT abid FROM usertab WHERE form_id = ?');
      $stmt2->execute([$_SESSION['uid']]);
      while($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
          $values['abilities'][$row['abid']] = TRUE;
      }

      printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  include('form.php');
}
else {
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    $values['fio'] = $_POST['fio'];

    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    $values['email'] = $_POST['email'];

    if (empty($_POST['bd'])) {
        setcookie('bd_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60);
    }
    $values['bd'] = $_POST['bd'];

    if (empty($_POST['sex'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
    }
    $values['sex'] = $_POST['sex'];

    if (empty($_POST['limbs'])) {
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    $values['limbs'] = $_POST['limbs'];

    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    $values['bio'] = $_POST['bio'];

    if (!isset($_POST['accept'])) {
        setcookie('accept_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    setcookie('abilities_value_1', '0', time() + 30 * 24 * 60 * 60);
    setcookie('abilities_value_2', '0', time() + 30 * 24 * 60 * 60);
    setcookie('abilities_value_3', '0', time() + 30 * 24 * 60 * 60);
    setcookie('abilities_value_4', '0', time() + 30 * 24 * 60 * 60);

    if (empty($_POST['abilities'])) {
        setcookie('abilities_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        foreach ($_POST['abilities'] as $super) {
            setcookie('abilities_value_' . $super, '1', time() + 30 * 24 * 60 * 60);
            $values['abilities'][$super] = TRUE;
        }
    }



    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('bd_error', '', 100000);
        setcookie('abilities_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('accept_error', '', 100000);

    }

    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $user = 'u20704';
        $pass = '6653476';
        $db = new PDO('mysql:host=localhost;dbname=u20704', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        $stmt1 = $db->prepare('UPDATE formtab5 SET fio=?, email=?, bd=?, sex=?, limbs=?, bio=? WHERE form_id = ?');
        $stmt1->execute([$values['fio'], $values['email'], $values['bd'], $values['sex'], $values['limbs'], $values['bio'], $_SESSION['uid']]);

        $stmt2 = $db->prepare('DELETE FROM usertab WHERE form_id = ?');
        $stmt2->execute([$_SESSION['uid']]);

        $stmt3 = $db->prepare("INSERT INTO usertab SET form_id = ?, abid = ?");
        foreach ($_POST['abilities'] as $s)
            $stmt3 -> execute([$_SESSION['uid'], $s]);
    }


    else {
        $id = uniqid();
        $hash = md5($id);
        $login = substr($hash, 0, 10);
        $pass = substr($hash, 10, 15);
        setcookie('login', $login);
        setcookie('pass', $pass);

        $user = 'u20704';
        $pass_db = '6653476';
        $db = new PDO('mysql:host=localhost;dbname=u20704', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
        $stmt1 = $db->prepare("INSERT INTO formtab5 SET fio = ?, email = ?, bd = ?, 
      sex = ? , limbs = ?, bio = ?, login = ?, pass = ?");
        $stmt1 -> execute([$_POST['fio'], $_POST['email'], $_POST['bd'],
            $_POST['sex'], $_POST['limbs'], $_POST['bio'], $login, $pass]);
        $stmt2 = $db->prepare("INSERT INTO usertab SET form_id = ?, abid = ?");
        $id = $db->lastInsertId();
        foreach ($_POST['abilities'] as $s)
            $stmt2 -> execute([$id, $s]);

    }

    setcookie('save', '1');

    header('Location: ./');
}
