<?php


header('Content-Type: text/html; charset=UTF-8');

session_start();
if (!empty($_SESSION['login'])) {
  header('Location: ./');
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['nologin']))
        print("<div>Пользователя с таким логином не существует</div>");
    if (!empty($_GET['wrongpass']))
        print("<div>Неверный пароль!</div>");

    ?>

<form action="" method="post">
  <input name="login" />
  <input name="pass" />
  <input type="submit" value="Войти" />
</form>

<?php
}
else {

    $user = 'u20704';
    $pass_db = '6653476';
    $db = new PDO('mysql:host=localhost;dbname=u20704', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
    $stmt1 = $db->prepare('SELECT form_id, pass FROM formtab5 WHERE login = ?');
    $stmt1->execute([$_POST['login']]);
    $row = $stmt1->fetch(PDO::FETCH_ASSOC);


    if (!$row) {
        header('Location: ?nologin=1');
        exit();
    }


    if ($row['pass'] != $_POST['pass']) {
        header('Location: ?wrongpass=1');
        exit();
    }

    $_SESSION['login'] = $_POST['login'];
    $_SESSION['uid'] = $row['form_id'];

    header('Location: ./');

}
